#! /usr/bin/env python3
# -*- coding:Utf8 -*-

"""setup.py file for HPEventList."""

# --------------------------------------------------------------------------------------------------------------
# All necessary import:
# --------------------------------------------------------------------------------------------------------------
# import versioneer

from setuptools import find_packages
from setuptools import setup
from doc.utils import APIDoc
from event_list import _version

packages = find_packages()

# --------------------------------------------------------------------------------------------------------------
# Call the setup function:
# --------------------------------------------------------------------------------------------------------------

cmdclass = {
    "build_doc": APIDoc,
}

setup(
    name='HPEventList',
    version=_version.__version__,
    description="Python 3 module for handling HPEventList",
    long_description=open("README.md").read(),
    author="B.Cecconi",
    author_email="baptiste.cecconi@obspm.fr",
    license="BSD",
    packages=packages,
    cmdclass=cmdclass,
#    entry_points={
#        "console_scripts": [
#            "event_list=even_list.script:main"]
#    },
    install_requires=['openpyxl>=2.3.5', 'numpy>=1.11.0',
                      'matplotlib', 'sphinx>=1.4.1', 'pytz',
                      'sphinx_rtd_theme'],
    include_package_data=True,
    url="https://gitlab.obspm.fr/ihdea/event-list"
)
