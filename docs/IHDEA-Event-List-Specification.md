# Specification for Heliophysics Event List (Catalogue) Format ("HPEventList" format)

__Version 1.0__

There are two forms of a standard Event List, one a straight ASCII table, 
and the other using the VOTable schema. They must be equivalent in content.

## ASCII table version: 

The Event List begins with single metadata lines that begin with `<Keywords>` 
where the possible keywords and their arguments are:
```
# EventTableVersion: HPEventList v1.0  http://spase-group.org/EventListSpec.txt
# ListTitle: <Title of the Event List>  
# ListID: <SPASE ID of list if available> 
# CreationDate: <ISO 8601 Date and Time of the Table creation>
# ModifyDate: <ISO 8601 Date and Time of the last modification of the list>
# FieldNames: <Ordered, space separated names of variables in list>
# FieldUnits: <Ordered, space separated units for the variables>
# FieldTypes: <Ordered, space separated types for the variables>
# FieldNulls: <Ordered, space separated fill values for the variables>
# ListBeginDate: <First possible ISO 8601 Date and Time for the List>  
# ListEndDate: <Last possible ISO 8601 Date and Time for the List>
# Contact: <Relevant contact person and possibly contact information>
# ContactID: <SPASE ID of contact person, if available>
# Description:   This is free text, and must be the last entry.  It may
# have multiple lines (unlike the above), each preceded by "#".
```

The description is followed immediately by the table values in rows, space 
separated, and with double quotes around any text entries containing spaces.

### Other requirements:

The `ListTitle`, `FieldNames`, `FieldUnits`, and `FieldTypes` are the only 
required lines, apart from the actual table entries. 

The first variable listed in `FieldNames` must be that for the beginning times 
of the events, and the second must be that for the end times.  If the events 
are considered to be instantaneous, then the end times should be entered as 
the same as the beginning times to avoid confusion with other times in the table. 

`FieldNames (variable names) may not contain spaces or special characters
except "_" (underscore).

`FieldUnits` may not contain spaces, but may contain "*", ".", "/", "^", "-", 
and "+".  The unit for ISO 8601 date/times must be "DateTime" and other text 
fields must have units of "text".

Possible `FieldTypes` are the VOTable types of: `boolean`, `short`, `int`, 
`long`, `char`, `float`, `double`, `floatComplex`, `doubleComplex`; other 
VOTable types are not allowed, i.e., `bit`, `unsignedByte`, and `unicodeChar`).  

All variables are required to have values, but `None` is allowed for units.

Times should all be given as ISO 8601 standard date/time 
(`yyyy-mm-ddThh:mm:ss.sZ`) and given a `DateTime` unit. The time value of
`0001-01-01T00:00:00.0Z` is considered a flag for an unknown or unspecified
time value (i.e., it is the FieldNull for DateTime variables). It should be 
used both in the header and the data as needed.

The `ListStartDate` and `ListStopDate` give the start and stop times of the 
surveyed period. All events must fall within this range. The time flag value 
should be used for time values that are not known or not relevant. If both 
values are given as the time flag value, the list is to be considered a 
non-exhaustive list of events.

### Further Notes:

Any description may be written after `# Description`. This may (should!)
include the way the Time Table has been generated or its origin if it has been 
adapted from an existing referenced Time Table. URLs to additional information 
are desirable.

The observatories and variables used to create the table should be identified 
and described well enough to be understood. If a SPASE Resource ID exists for 
a referenced item it should be included in the description.

Any table based on a well-developed procedure should provide an information 
URL on the procedure and a contact person or group. Once registered, a 
SPASE Resource ID should be given as the event list name.

There are no restrictions or standards for the contents of the columns of the 
table after the second.  Strings in the table should either not contain spaces 
or other standard separators (preferable), or should be in quotes.

All fields should be filled either with data entries or with fill values of 
the same type as the quantities (from forth line). 

## VOTable Version:

See [VOTable v1.2](http://www.ivoa.net/documents/VOTable/20091130/REC-VOTable-1.2.html)
for VOTable specifications of relevance, e.g., for types ("primitives").

The VOTable version of the HPEvent formatted lists has all the same metadata 
but in a different form.  

The tags that begin with `Field` are mapped to VOTable Fields, with each
variable in the list corresponding to a separate field.  The beginning and end 
times of the events must be the first two Fields.  The syntax is:
```
<FIELD datatype="<FieldType>" unit="<FieldUnit>" name="<FieldName.">
        <VALUES null="<FieldNull>"/>
```
where the `FieldTypes`, etc. correspond sequentially to those in the ASCII 
table lines.

Fields that have units of `text` must also have an arraysize:
```
<FIELD arraysize="*" datatype="char" unit="text" name="<FieldName>">
        <VALUES null="<FieldNull>"/>
</FIELD>
```
and those with a unit of `DateTime` need a `ucd` and type: 
```
<FIELD arraysize="*" datatype="char" xtype="timestamp" name="<FieldName>" ucd="time.epoch">
        <VALUES null="<FieldNull>"/>
</FIELD>
```
The `ListTitle` maps to:
```
<RESOURCE name="<ListName>">
```
with `</RESOURCE>` closing the block at the end of the actual table entries.

All other tags in the ASCII table version are mapped to VOTable Parameters, 
given my `<PARAM ... />`.


The actual text of the Description is contained in a `CDATA` block:
```
      <DESCRIPTION>
<![CDATA[
        Test of the description goes here with the same content
        as in the ASCII table version.
]]>
      </DESCRIPTION>
```
The column entries in the flat ASCII version of the HPEventList lists are 
replaced by tagged tables, with each row delimited by `<TR> ... </TR>` and the 
row entries by `<TD> ... </TD>`.  

