# IHDEA Event List

This repository deals with the [IHDEA](https://ihdea.net) (International 
Heliophysics Data Environment Alliance) Event List standard proposal. This 
standard was initially prepared in 2012 in the frame of the NASA/HDMC (Heliophysics 
Data and Model Consortium) and EC funded HELIO (Heliphysics Integrated Observatory) project.

## Event list formats

Two formats are proposed: ASCII table and Votable 

| Format       | Mime-Type                  | Note                   | 
|--------------|----------------------------|------------------------|
| ASCII table  | `text/plain`               | CSV data table, with header |
| Votable      | `application/xml+votable`  | Compliant with [VOTable v1.2](http://www.ivoa.net/documents/VOTable/20091130/REC-VOTable-1.2.html) |

## Detailed documentaion

See [IHDEA-Event-List-Specification.md](docs/IHDEA-Event-List-Specification.md)

## Examples

Table from _Mason et al. Sol. Phys. 2009_
[doi:10.1007/s11207-009-9367-0](https://doi.org/10.1007/s11207-009-9367-0)

- Text version: [mason_cir_list_2009.txt](tests/data/mason_cir_list_2009.txt)
- VOTable version: [mason_cir_list_2009.xml](tests/data/mason_cir_list_2009.xml)


