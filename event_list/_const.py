#! /usr/bin/env python
# -*- coding: latin-1 -*-

"""
Python module for `IHDEA <https://ihdea.net>`_ HPEventList.

This module defines constant variables for the HPEventList module.

@author: B.Cecconi(ObsParis/LESIA)
"""

__author__ = "Baptiste Cecconi"
__copyright__ = "Copyright 2019, LESIA-PADC, Observatoire de Paris"
__credits__ = ["Baptiste Cecconi"]
__license__ = "MIT"
__version__ = "1.0"
__maintainer__ = "Baptiste Cecconi"
__email__ = "baptiste.cecconi@obspm.fr"
__status__ = "Production"
__date__ = "07-FEB-2019"
__project__ = "IHDEA/HPEventList"

import datetime
import dateutil.parser
import numpy

__all__ = ["_ingest_conversion", "_dtype_conversion", "_field_keys",
           "_param_keys", "_param_types", "_param_utypes", "_param_ucds",
           "_isotime_format", "_votable_version"]

_ingest_conversion = {
    'boolean': bool,
    'short': int,
    'int': int,
    'long': int,
    'char': str,
    'float': float,
    'double': float,
    'floatComplex': complex,
    'doubleComplex': complex,
    'DateTime': dateutil.parser.parse,
}

_dtype_conversion = {
    'boolean': numpy.bool,
    'short': numpy.int16,
    'int': numpy.int32,
    'long': numpy.int64,
    'char': numpy.unicode,
    'float': numpy.float32,
    'double': numpy.float64,
    'floatComplex': numpy.complex,
    'doubleComplex': numpy.complex64,
    'DateTime': numpy.object,
}

_field_keys = ['FieldNames', 'FieldTypes', 'FieldUnits', 'FieldNulls']

_param_keys = ['ListTitle', 'ListID', 'CreationDate', 'ModifyDate', 'Contact', 'ContactID', 'Description',
               'ListBeginDate', 'ListEndDate']
_param_types = [str, str, datetime.datetime, datetime.datetime, str, str, str, datetime.datetime, datetime.datetime]
_param_utypes = ['spase:ResourceHeader/ResourceName', 'spase:Catalog/ResourceID', 'vr:Resource.created',
                 'vr:Resource.updated', 'spase:Person/PersonName', 'spase:Person/ResourceID',
                 'spase:ResourceHeader/Description', 'spase:TimeSpan/StartDate', 'spase:TimeSpan/StopDate']
_param_ucds = ['meta.title', 'meta.id', 'time.creation', 'time.processing', 'meta.id', 'meta.id', 'meta.note',
               'time.start', 'time.end']

_isotime_format = '%Y-%m-%dT%H:%M:%S.%f'

_votable_version = '1.2'
