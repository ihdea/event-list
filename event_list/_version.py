#! /usr/bin/env python
# -*- coding: latin-1 -*-

"""
Python module for `IHDEA <https://ihdea.net>`_ HPEventList.

This module defines the version and release history of the HPEventList module.

@author: B.Cecconi(ObsParis/LESIA)
"""

__version__ = "0.1.0"
__date__ = "01-MAR-2019"
__change__ = {"0.1.0": "First beta release"}
