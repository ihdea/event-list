#! /usr/bin/env python
# -*- coding: latin-1 -*-

"""
Python module for `IHDEA <https://ihdea.net>`_ HPEventList.

This module defines the HPEventList main class.

@author: B.Cecconi(ObsParis/LESIA)
"""

__author__ = "Baptiste Cecconi"
__copyright__ = "Copyright 2019, LESIA-PADC, Observatoire de Paris"
__credits__ = ["Baptiste Cecconi"]
__license__ = "MIT"
__version__ = "1.0"
__maintainer__ = "Baptiste Cecconi"
__email__ = "baptiste.cecconi@obspm.fr"
__status__ = "Production"
__date__ = "07-FEB-2019"
__project__ = "IHDEA/HPEventList"

# import modules
import collections
from io import BytesIO, TextIOWrapper
import xml.etree.ElementTree as ET
import datetime
import dateutil.parser
from astropy.io.votable import parse as votable_parse
from astropy.io.votable.tree import VOTableFile, Resource as vResource, Table as vTable, \
    Field as vField, Param as vParam
from astropy.io import ascii
from astropy import units as u
from astropy.table import Table, Column
import os
import sys
import shutil
from ._const import *
from ._version import __version__

# module interface
__all__ = ['HPEventList']


# =============================
# Local Variables and Functions
# =============================


def _to_isotime_format(dt):
    return "{}Z".format(dt.strftime(_isotime_format)[0:21])


def _get_version():
    return 'HPEventList v1.0 http://spase-group.org/EventListSpec.txt (Python v{}, IHDEA/HPEventList v{})'. \
        format(sys.version[:3], __version__)

# ============
# Module Class
# ============


class HPEventList(Table):

    def __init__(self, data=None, masked=None, names=None, dtype=None, meta=None, copy=True, rows=None,
                 copy_indices=True):
        """The HPEventList class, inheriting for `astropy.table.Table <http://docs.astropy.org/en/stable/table/>`_.

        :param data (numpy.ndarray, dict, list, Table, or table-like object): (optional) Data to initialize table.
        :param masked (bool): (optional) Specify whether the table is masked.
        :param names (list): (optional) Specify column names.
        :param dtype (list): (optional)  Specify column data types.
        :param meta (dict): (optional) Metadata associated with the table.
        :param copy (bool): (optional) Copy the input data. If the input is a Table the ``meta`` is always copied
        regardless of the ``copy`` parameter. Default is True.
        :param rows (numpy.ndarray, list of lists): (optional) Row-oriented data for table instead of ``data`` argument.
        :param copy_indices (bool): (optional) Copy any indices in the input data. Default is True.
        """

        # Initialize with data input parameter, which must be an astropy.table.Table object or similar
        Table.__init__(self, data, masked=masked)

        # Generic metadata attributes
        if self.meta is None:
            self.meta['version'] = _get_version()
            self.meta['fields'] = collections.OrderedDict()
            self.meta['params'] = collections.OrderedDict()

    def _fix_fields(self):

        # Fixing empty FieldNulls:
        if self.meta['fields']['FieldNulls'] is None:
            self.meta['fields']['FieldNulls'] = [None] * len(self.meta['fields']['FieldNames'])

    def _fix_params(self):

        def _fix_date(date_meta):
            if date_meta is None:
                date_meta = datetime.datetime.now(tz=None)
            elif isinstance(date_meta, str):
                date_meta = dateutil.parser.parse(date_meta)
            return date_meta

        # Fixing CreationDate:
        self.meta['params']['CreationDate'] = _fix_date(self.meta['params']['CreationDate'])

        # Fixing ModifyDate:
        self.meta['params']['ModifyDate'] = _fix_date(self.meta['params']['ModifyDate'])

        # Fixing ListBeginDate, ListEndDate:
        self.meta['params']['ListBeginDate'] = dateutil.parser.parse("9999-12-31T23:59:59.999Z")
        self.meta['params']['ListEndDate'] = dateutil.parser.parse("0001-01-01T00:00:00.000Z")
        for row_index in range(len(self)):
            row = self[row_index]
            for cur_field, cur_type in zip(self.meta['fields']['FieldNames'], self.meta['fields']['FieldTypes']):
                if cur_type == 'DateTime':
                    self.meta['params']['ListBeginDate'] = min(self.meta['params']['ListBeginDate'], row[cur_field])
                    self.meta['params']['ListEndDate'] = max(self.meta['params']['ListEndDate'], row[cur_field])

    def _table_conversion_helper(self):
        table_length = len(self)
        # We convert each column of the Table with the correct Field types
        for cur_key, cur_type in zip(self.meta['fields']['FieldNames'], self.meta['fields']['FieldTypes']):
            col_index = self.index_column(cur_key)
            tmp_col = self[cur_key]
            new_col = Column(name=cur_key, dtype=_dtype_conversion[cur_type], length=table_length)
            for cur_index in range(table_length):
                new_col[cur_index] = _ingest_conversion[cur_type](tmp_col[cur_index])
            self.remove_column(cur_key)
            self.add_column(new_col, col_index)

    def _fix_votable(self, buffer):

        def _fix_values_null(cur_element):
            if cur_element.tag == '{{http://www.ivoa.net/xml/VOTable/v{}}}VALUES'.format(_votable_version):
                if 'null' in cur_element.attrib.keys():
                    if cur_element.attrib['null'].startswith("b'"):
                        cur_element.set('null', cur_element.attrib['null'][2:-1])

        def _fix_xtype_timestamp(cur_element):
            cur_element.set('xtype', 'timestamp')
            cur_element.set('unit', '')

        buffer.seek(0)
        tree = ET.parse(buffer)
        root = tree.getroot()

        # Fixing PARAMS
        for cur_param in root.iter('{{http://www.ivoa.net/xml/VOTable/v{}}}PARAM'.format(_votable_version)):
            if cur_param.attrib['ID'].endswith('Date'):
                _fix_xtype_timestamp(cur_param)
            for child in cur_param:
                _fix_values_null(child)

        # Fixing FIELDS
        for cur_field in root.iter('{{http://www.ivoa.net/xml/VOTable/v{}}}FIELD'.format(_votable_version)):
            if 'unit' in cur_field.attrib.keys():
                if cur_field.attrib['unit'] == 'DateTime':
                    _fix_xtype_timestamp(cur_field)
            for child in cur_field:
                _fix_values_null(child)

        buffer.seek(0)
        tree.write(buffer)

    @classmethod
    def from_params(cls, file_name: str, list_title: str, list_id: str, field_names: list, field_types: list,
                  field_units: list, field_nulls: list, data: Table, creation_date: datetime.datetime,
                  modify_date: datetime.datetime, description: str, contact: str, contact_id: str,
                  list_begin_date: datetime.datetime, list_end_date: datetime.datetime):
        """
        :param str file_name: File Name
        :param str list_title: List Title
        :param str list_id: List SPASE Resource ID
        :param list field_names: Field Names
        :param list field_types: Field Types
        :param list field_units: Field Units
        :param list field_nulls: Field Nulls
        :param astropy.table.Table data: data table
        :param datetime.datetime creation_date: List creation date
        :param datetime.datetime modify_date: List modification date
        :param str description: Description text
        :param str contact: Contact name
        :param str contact_id: Contact id (SPASE Contact Resource ID)
        :param datetime.datetime list_begin_date: Earliest date of event list
        :param datetime.datetime list_end_date: Latest date of event list
        :return: HPEventList object
        :rtype: HPEventList
        """

        self = cls(data)

        self.meta['version'] = _get_version()
        self.meta['file_name'] = file_name
        self.meta['input_format'] = None

        # Data Fields attributes
        self.meta['fields'] = collections.OrderedDict([
            ('FieldNames', field_names),
            ('FieldTypes', field_types),
            ('FieldUnits', field_units),
            ('FieldNulls', field_nulls),
        ])
        self._fix_fields()

        # Fixing column types
        self._table_conversion_helper()

        # Header Params attributes
        self.meta['params'] = collections.OrderedDict([
            ('ListTitle', list_title),
            ('ListID', list_id),
            ('ListBeginDate', list_begin_date),
            ('ListEndDate', list_end_date),
            ('CreationDate', creation_date),
            ('ModifyDate', modify_date),
            ('Contact', contact),
            ('ContactID', contact_id),
            ('Description', description),
        ])
        self._fix_params()

        return self

    @classmethod
    def from_text(cls, file_name: str):
        """This class method can be used to import Text-formatted HPEventList.

        :param file_name: (str) path to the file
        :return: HPEventList object.
        :rtype: HPEventList
        """
        # TODO: include *_error columns as uncertainty (here or in main class)
        # TODO: include units in table (here or in main class)

        # initialize header dictionary with know Params and Fields
        header = dict()
        for cur_key in _field_keys:
            header[cur_key] = None
        for cur_key in _param_keys:
            header[cur_key] = None

        # We read extract the header and data parts into separate lists
        raw_comment = list()
        raw_data = list()
        with open(file_name, 'r') as f:
            for line in f.readlines():
                if line.startswith('# '):
                    raw_comment.append(line)
                else:
                    raw_data.append(line)

        # Analyzing header section
        desc = False  # this a flag for the description parameter which can be multi-line
        for line in raw_comment:

            # Remove the first 2 characters ("# ") and split with ":", # keep first element for as the header item key
            cur_key = line[2:].split(':')[0]
            # take the rest, strip it, and keep it as the header item value
            cur_val = line[4+len(cur_key):].strip()

            # Description item is processed differently
            if cur_key != 'Description' and not desc:
                header[cur_key] = cur_val
            elif desc:
                header['Description'] = header['Description'] + line[2:].strip()
            else:
                header['Description'] = cur_val
                desc = True

        # Create lists for Field names, types, units and nulls, from header
        name_list = [kk.strip() for kk in header['FieldNames'].split(',')]
        type_list = [tt.strip() for tt in header['FieldTypes'].split(',')]
        unit_list = [uu.strip() for uu in header['FieldUnits'].split(',')]
        null_list = None
        if 'FieldNulls' in header.keys():
            null_list = [nn.strip() for nn in header['FieldNulls'].split(',')]

        # Now parse data into a Table object
        raw_data.insert(0, header['FieldNames'])

        # We use the ascii.read parser from astropy.io
        tmp_table = ascii.read(raw_data, format='csv')

        # The output is an instance of HPEventList
        h = cls.from_params(file_name, header['ListTitle'], header['ListID'], name_list, type_list, unit_list,
                            null_list, tmp_table, header['CreationDate'], header['ModifyDate'], header['Description'],
                            header['Contact'], header['ContactID'], header['ListBeginDate'], header['ListEndDate'])
        h.meta['input_format'] = 'text/plain'
        return h

    @classmethod
    def from_votable(cls, file_name):
        """This class method can be used to import VOTable-formatted HPEventList.

        :param file_name: (str) path to the file
        :return: HPEventList object.
        :rtype: HPEventList
        """

        vot = votable_parse(file_name)
        tab = vot.get_first_table()

        # Extracting Field metadata
        field_names = tab.to_table().keys()
        field_types = []
        field_units = []
        field_nulls = []
        for cur_item in field_names:
            cur_field = tab.get_field_by_id(cur_item)
            if cur_field.xtype is not None:
                if cur_field.xtype.lower() == 'timestamp':
                    field_types.append('DateTime')
            else:
                field_types.append(cur_field.datatype)
            field_units.append(cur_field.unit)
            if isinstance(cur_field.values.null, bytes):
                field_nulls.append(cur_field.values.null.decode('ascii'))
            else:
                field_nulls.append(cur_field.values.null)

        # Extracting Param metadata
        header = dict()
        for cur_item in _param_keys:
            header[cur_item] = None

        for cur_item in tab.iter_fields_and_params():
            if cur_item.name not in field_names:
                cur_index = _param_keys.index(cur_item.name)
                if isinstance(cur_item.value, bytes):
                    if _param_types[cur_index] == datetime.datetime:
                        header[cur_item.name] = dateutil.parser.parse(cur_item.value.decode('ascii'))
                    else:
                        header[cur_item.name] = cur_item.value.decode('ascii')
                else:
                    header[cur_item.name] = cur_item.value

        h = cls.from_params(file_name, vot.resources[0].name, header['ListID'], field_names, field_types, field_units,
                            field_nulls, tab.to_table(), header['CreationDate'], header['ModifyDate'],
                            tab.description, header['Contact'], header['ContactID'], header['ListBeginDate'],
                            header['ListEndDate'])
        h.meta['input_format'] = 'application/xml+votable'
        return h

    def to_votable(self, file_name='output.xml', force_rewrite=False, return_votable=False):
        """This methods writes out the HPEventList into a votable-formatted HPEVentList file.

        :param file_name: (str) the output file name (default = 'output.xml')
        :param force_rewrite: (bool) set to True to overwrite an existing file (default = False)
        :param return_votable: (bool) set to True to return the votable object instead of writing file (default = False)
        """

        # Checking output mode:
        if not return_votable:

            # Checking overwrite
            if os.path.exists(file_name) and not force_rewrite:
                raise FileExistsError("This file already exists, use force_rewrite=True option to overwrite.")

            # Checking Filename extension
            if not file_name.endswith('.xml'):
                raise ValueError("The file name extension must be .xml")

        # Create VOTable object
        votable = VOTableFile(version=_votable_version)

        # Adding Resource section
        res = vResource()
        if self.meta['params']['ListID'] is not None:
            res.name = self.meta['params']['ListID']
        votable.resources.append(res)

        # Adding Resource/Table
        tab = vTable(votable)
        res.tables.append(tab)
        if self.meta['params']['Description'] is not None:
            tab.description = self.meta['params']['Description']

        # Adding Resource/Table/Params
        for cur_param, cur_type, cur_utype, cur_ucd in zip(_param_keys, _param_types, _param_utypes, _param_ucds):
            if cur_param not in ['Description', 'ListID'] and self.meta['params'][cur_param] is not None:
                tmp_param = vParam(votable, name=cur_param, utype=cur_utype, arraysize='*', datatype='char',
                                   ucd=cur_ucd, value=self.meta['params'][cur_param])
                if cur_type == datetime.datetime:
                    tmp_param.value = _to_isotime_format(self.meta['params'][cur_param])
                    tmp_param.xtype = "timestamp"
                    tmp_param.values.null = b'0001-01-01T00:00:00.0Z'

                tab.params.append(tmp_param)

        # Adding Resource/Table/Fields
        for cur_field, cur_type, cur_unit, cur_null in zip(self.meta['fields']['FieldNames'],
                                                           self.meta['fields']['FieldTypes'],
                                                           self.meta['fields']['FieldUnits'],
                                                           self.meta['fields']['FieldNulls']):
            if cur_type == 'DateTime':
                tmp_field = vField(votable, name=cur_field, datatype='char', xtype='dateTime', arraysize='*')
            elif cur_type == 'char':
                tmp_field = vField(votable, name=cur_field, datatype='char', arraysize='*')
            else:
                tmp_field = vField(votable, name=cur_field, datatype=cur_type)
            if cur_unit != 'None':
                tmp_field.unit = cur_unit
            if cur_null != 'None':
                tmp_field.values.null = cur_null
            tab.fields.append(tmp_field)

        tab.create_arrays(len(self))
        for row_index in range(len(self)):
            for cur_field, cur_type in zip(self.meta['fields']['FieldNames'], self.meta['fields']['FieldTypes']):
                if cur_type == 'DateTime':
                    tab.array[cur_field][row_index] = _to_isotime_format(self[cur_field][row_index])
                else:
                    tab.array[cur_field][row_index] = self[cur_field][row_index]

        # output (return or file)
        if return_votable:
            return votable
        else:
            file_buffer = BytesIO()
            votable.to_xml(file_buffer)
            file_buffer.flush()
            self._fix_votable(file_buffer)
            with open(file_name, 'wb') as fd:
                file_buffer.seek(0)
                shutil.copyfileobj(file_buffer, fd)
            file_buffer.close()

    def to_text(self, file_name='output.txt', force_rewrite=False):
        """This methods writes out the HPEventList into a text-formatted HPEVentList file.

        :param file_name: (str) the output file name (default = 'output.txt')
        :param force_rewrite: (bool) set to True to overwrite an existing file (default = False)
        """

        # Checking overwrite
        if os.path.exists(file_name) and not force_rewrite:
            raise FileExistsError("This file already exists, use force_rewrite=True option to overwrite.")

        # Checking Filename extension
        if not file_name.endswith('.txt'):
            raise ValueError("The file name extension must be .txt")

        # Writing output to file
        with open(file_name, 'w') as f:

            # Writing out header section
            f.write("# EventTableVersion: {}\n".format(self.meta['version']))
            f.write("# ListTitle: {}\n".format(self.meta['params']['ListTitle']))
            if self.meta['params']['ListID'] is not None:
                f.write('# ListID: {}\n'.format(self.meta['params']['ListID']))
            f.write("# CreationDate: {}\n".format(self.meta['params']['CreationDate']))
            f.write("# ModifyDate: {}\n".format(self.meta['params']['ModifyDate']))
            for cur_key in self.meta['fields'].keys():
                if self.meta['fields'][cur_key] is not None:
                    f.write("# {}: {}\n".format(cur_key,
                                                ','.join([str(item) for item in self.meta['fields'][cur_key]])))
            f.write('# ListBeginDate: {}\n'.format(_to_isotime_format(self.meta['params']['ListBeginDate'])))
            f.write('# ListEndDate: {}\n'.format(_to_isotime_format(self.meta['params']['ListEndDate'])))
            if self.meta['params']['Contact'] is not None:
                f.write('# Contact: {}\n'.format(self.meta['params']['Contact']))
            if self.meta['params']['ContactID'] is not None:
                f.write('# ContactID: {}\n'.format(self.meta['params']['ContactID']))
            if self.meta['params']['Description'] is not None:
                f.write('# Description: {}\n'.format(self.meta['params']['Description']))

            # Writing out data section
            for row_index in range(len(self)):
                row = self[row_index]
                tmp_row = []
                for cur_field, cur_type in zip(self.meta['fields']['FieldNames'], self.meta['fields']['FieldTypes']):
                    if cur_type == 'DateTime':
                        tmp_row.append('{}'.format(_to_isotime_format(row[cur_field])))
                    elif isinstance(row[cur_field], u.quantity.Quantity):
                        tmp_row.append(str(row[cur_field].value))
                    else:
                        tmp_row.append(str(row[cur_field]))
                f.write(','.join(tmp_row)+'\n')
