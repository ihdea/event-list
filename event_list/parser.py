#! /usr/bin/env python
# -*- coding: latin-1 -*-

"""
Python module for `IHDEA <https://ihdea.net>`_ HPEventList.

This module defines the parsers of the HPEventList module.

@author: B.Cecconi(ObsParis/LESIA)
"""


__author__ = "Baptiste Cecconi"
__copyright__ = "Copyright 2019, LESIA-PADC, Observatoire de Paris"
__credits__ = ["Baptiste Cecconi"]
__license__ = "MIT"
__version__ = "1.0"
__maintainer__ = "Baptiste Cecconi"
__email__ = "baptiste.cecconi@obspm.fr"
__status__ = "Production"
__date__ = "07-FEB-2019"
__project__ = "IHDEA/HPEventList"

# import modules
import os
from ._class import HPEventList
from pathlib import Path
import errno

# module interface
__all__ = ['parse_votable', 'parse_text']

# ===============================
# Private functions
# ===============================


def _file_check(file_format):
    if file_format == 'votable':
        return '<'
    elif file_format == 'text':
        return '#'
    else:
        raise ValueError('Wrong file format.')


def _file_methods(file_format):
    if file_format == 'votable':
        return HPEventList.from_votable
    elif file_format == 'text':
        return HPEventList.from_text
    else:
        raise ValueError('Wrong file format.')


def _parse_file(file_name, file_format):

    # check if file_name is a Path object or not
    if isinstance(file_name, Path):
        path_exists = file_name.exists()
        cur_file_name = str(file_name)
    else:
        path_exists = os.path.exists(file_name)
        cur_file_name = file_name

    # sanity check of file format
    if path_exists:
        with open(cur_file_name, 'r') as f:
            test = f.read(1)
            if test != _file_check(file_format):
                raise ValueError("The selected file is not a valid HPEventList {} file.".format(file_format))
    else:
        raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), cur_file_name)

    # run the right parse method for this file format
    return _file_methods(file_format)(cur_file_name)


# ===============================
# Global functions
# ===============================

def parse_votable(file_name):
    """VOTable format HPEventList parser

    :param file_name (str or pathlib.Path): Path to the input file. Must be a valid VOTable file, following the
    HPEventList specification.
    :return: HPEventList object.
    """
    return _parse_file(file_name, 'votable')


def parse_text(file_name):
    """Text format HPEventList parser

    :param file_name (str or pathlib.Path): Path to the input file. Must be a valid text file, following the HPEventList
    specification.
    :return: HPEventList object.
    """
    return _parse_file(file_name, 'text')
