#! /usr/bin/env python
# -*- coding: latin-1 -*-

"""
Python module for `IHDEA <https://ihdea.net>`_ HPEventList.

This projects was initially drafted through the NASA/HDMC (Heliophysics Data and Model Consortium) and EC-funded
HELIO (Heliophysics Integrated Observatory) project.

This implementation uses the `astropy.table.Table <http://docs.astropy.org/en/stable/table/>`_ internal representation.

@author: B.Cecconi (ObsParis/LESIA)
"""

__author__ = "Baptiste Cecconi"
__copyright__ = "Copyright 2019, LESIA-PADC, Observatoire de Paris"
__credits__ = ["Baptiste Cecconi"]
__license__ = "MIT"
__version__ = "1.0"
__maintainer__ = "Baptiste Cecconi"
__email__ = "baptiste.cecconi@obspm.fr"
__status__ = "Production"
__date__ = "07-FEB-2019"
__project__ = "IHDEA/HPEventList"

from ._class import HPEventList
from . import parser


