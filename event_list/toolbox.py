#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""Toolbox module for HPEventList package."""

# ________________ IMPORT _________________________
# (Include here the modules to import, e.g. import sys)
import logging

# ________________ Global Variables _____________
# (define here the global variables)
logger = logging.getLogger(__name__)


def setup_logging(filename=None,
                  quiet=False, verbose=False, debug=False):
    """Method to set up logging."""
    if debug:
        logging.basicConfig(level=logging.DEBUG,
                            format='%(levelname)-8s: %(message)s')
    elif verbose:
        logging.basicConfig(level=logging.INFO,
                            format='%(levelname)-8s: %(message)s')
    else:
        logging.basicConfig(level=logging.ERROR,
                            format='%(levelname)-8s: %(message)s')

    if quiet:
        logging.root.handlers[0].setLevel(logging.CRITICAL + 10)
    elif verbose:
        logging.root.handlers[0].setLevel(logging.INFO)
    elif debug:
        logging.root.handlers[0].setLevel(logging.DEBUG)
    else:
        logging.root.handlers[0].setLevel(logging.ERROR)

    if filename:
        fh = logging.FileHandler(filename, delay=True)
        fh.setFormatter(logging.Formatter('%(asctime)s %(name)-\
                        12s %(levelname)-8s %(funcName)-12s %(message)s',
                                          datefmt='%Y-%m-%d %H:%M:%S'))
        if debug:
            fh.setLevel(logging.DEBUG)
        else:
            fh.setLevel(logging.INFO)

        logging.root.addHandler(fh)
