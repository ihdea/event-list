import unittest
import event_list
from . import test_files


text_files = []
votable_files = []
for cur_list in test_files.keys():
    if 'text' in test_files[cur_list].keys():
        text_files.append(test_files[cur_list]['text'])
    if 'votable' in test_files[cur_list].keys():
        votable_files.append(test_files[cur_list]['votable'])


class HPEventListParseText(unittest.TestCase):

    """test case for HPEventList Text Parser"""

    def test_text_parser(self):
        for cur_list in text_files:
            h = event_list.parser.parse_text(cur_list)
            self.assertIsInstance(h, event_list.HPEventList)

    def test_text_parser_file_not_found(self):
        with self.assertRaises(FileNotFoundError) as cm:
            h = event_list.parser.parse_text('not_a_file.txt')
        self.assertEqual(cm.exception.filename, 'not_a_file.txt')

    def test_text_parser_file_format(self):
        with self.assertRaises(ValueError) as cm:
            h = event_list.parser.parse_text(votable_files[0])


class HPEventListParseVotable(unittest.TestCase):

    """test case for HPEventList Text Parser"""

    def test_votable_parser(self):
        for cur_list in votable_files:
            h = event_list.parser.parse_votable(cur_list)
            self.assertIsInstance(h, event_list.HPEventList)

    def test_votable_parser_file_not_found(self):
        with self.assertRaises(FileNotFoundError) as cm:
            h = event_list.parser.parse_votable('not_a_file.xml')
        self.assertEqual(cm.exception.filename, 'not_a_file.xml')

    def test_votable_parser_file_format(self):
        with self.assertRaises(ValueError) as cm:
            h = event_list.parser.parse_votable(text_files[0])
