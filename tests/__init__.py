from pathlib import Path

test_data_directory = Path(__file__).parent / 'data'

test_files = {
    'Mason_CIR_2009': {
        'text': test_data_directory / 'mason_cir_list_2009.txt',
        'votable': test_data_directory / 'mason_cir_list_2009.xml'
    },
}